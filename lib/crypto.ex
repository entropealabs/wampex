defmodule Wampex.Crypto do
  @moduledoc false

  def hash_challenge(key, data) do
    :hmac
    |> :crypto.mac(:sha256, key, data)
    |> Base.encode64()
  end

  def pbkdf2(secret, salt, iterations, keylen) do
    :sha256
    |> :crypto.pbkdf2_hmac(secret, salt, iterations, keylen)
    |> Base.encode64()
  end

  def random_string(length) do
    length
    |> :crypto.strong_rand_bytes()
    |> Base.encode64()
  end

  def compare_secure(x, y) when is_binary(x) and is_binary(y) do
    compare_secure(String.to_charlist(x), String.to_charlist(y))
  end

  def compare_secure(x, y) when is_list(x) and is_list(y) do
    if length(x) == length(y) do
      compare_secure(x, y, 0)
    else
      false
    end
  end

  def compare_secure(_, _), do: false

  defp compare_secure([x | rest_x], [y | rest_y], acc) do
    compare_secure(rest_x, rest_y, Bitwise.bxor(x, y) |> Bitwise.bor(acc))
  end

  defp compare_secure([], [], acc), do: acc == 0
end
