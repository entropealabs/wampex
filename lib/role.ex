defmodule Wampex.Role do
  @moduledoc "Behaviour for Roles"
  @callback add(map()) :: map()
  @callback handle(message :: Wampex.message()) ::
              {[:gen_statem.action()], integer() | nil, Wampex.handle_response()}
end
