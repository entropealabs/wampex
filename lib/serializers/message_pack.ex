defmodule Wampex.Serializers.MessagePack do
  @moduledoc "MessgePack Serializer"
  @behaviour Wampex.Serializer

  @impl true
  def data_type, do: :binary
  @impl true
  def serialize!(data), do: Msgpax.pack!(data, iodata: false)
  @impl true
  def serialize(data), do: Msgpax.pack(data, iodata: false)
  @impl true
  def deserialize!(data), do: Msgpax.unpack!(data)
  @impl true
  def deserialize(data), do: Msgpax.unpack(data)
end
