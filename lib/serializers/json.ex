defmodule Wampex.Serializers.JSON do
  @moduledoc "JSON Serializer"
  @behaviour Wampex.Serializer

  @impl true
  def data_type, do: :text
  @impl true
  def serialize!(data), do: Jason.encode!(data)
  @impl true
  def serialize(data), do: Jason.encode(data)
  @impl true
  def deserialize!(binary), do: Jason.decode!(binary)
  @impl true
  def deserialize(binary), do: Jason.decode(binary)
end
