defmodule Wampex do
  @moduledoc """
  Types for wampex
  """
  alias Wampex.Roles
  alias Roles.{Broker, Callee, Caller, Dealer, Peer, Publisher, Subscriber}
  alias Broker.{Event, Published, Subscribed, Unsubscribed}
  alias Callee.{Register, Unregister, Yield}
  alias Caller.Call
  alias Publisher.Publish
  alias Subscriber.{Subscribe, Unsubscribe}
  alias Dealer.{Invocation, Registered, Result, Unregistered}
  alias Peer.{Authenticate, Challenge, Error, Goodbye, Hello}

  @type message_part :: integer() | binary() | map() | list()
  @type message :: nonempty_list(message_part())
  @type arg_list :: [] | nonempty_list(any())
  @type arg_keyword :: map()

  @type error ::
          {:error, reason :: binary()}
          | Error.t()
  @type messages ::
          Publish.t()
          | Challenge.t()
          | Goodbye.t()
          | Published.t()
          | Hello.t()
          | Authenticate.t()
          | Unsubscribe.t()
          | Unsubscribed.t()
          | Subscribe.t()
          | Subscribed.t()
          | Invocation.t()
          | Register.t()
          | Registered.t()
          | Unregister.t()
          | Unregistered.t()
          | Call.t()
          | Yield.t()
          | Result.t()
          | Error.t()
          | Event.t()

  @type handle_response ::
          {:ok, integer()}
          | {:ok, details :: map(), arg_list :: arg_list(), arg_kw :: arg_keyword()}
          | {:update, atom(), messages()}
          | messages()
end
