defmodule WampexTest do
  use ExUnit.Case, async: true
  doctest Wampex

  alias Wampex.Roles.{Broker, Callee, Caller, Dealer, Peer, Publisher, Subscriber}
  alias Wampex.Serializers.{JSON, MessagePack}
  alias Broker.{Event, Published, Subscribed, Unsubscribed}
  alias Callee.{Register, Unregister, Yield}
  alias Caller.Call
  alias Dealer.{Invocation, Registered, Result, Unregistered}
  alias Peer.{Authenticate, Error, Goodbye, Hello}
  alias Publisher.Publish
  alias Subscriber.{Subscribe, Unsubscribe}
  require Logger

  test "Callee.add" do
    assert %{callee: %{}} = Callee.add(%{})
  end

  test "Callee.invocation_error" do
    assert [8, 68, 1234, %{}, "error", [], %{}] =
             Callee.invocation_error(%Error{request_id: 1234, error: "error"})
  end

  test "Callee.register" do
    assert [64, %{}, "test"] = Callee.register(%Register{procedure: "test"})
  end

  test "Callee.unregister" do
    assert [66, 123_456] = Callee.unregister(%Unregister{registration_id: 123_456})
  end

  test "Callee.yield" do
    assert [70, 1234, %{}, [], %{}] = Callee.yield(%Yield{request_id: 1234})
    assert [70, 1234, %{}, ["1"], %{}] = Callee.yield(%Yield{request_id: 1234, arg_list: ["1"]})

    assert [70, 1234, %{test: 1}, [2], %{}] =
             Callee.yield(%Yield{
               request_id: 1234,
               arg_list: [2],
               arg_kw: %{},
               options: %{test: 1}
             })
  end

  test "Caller.add" do
    assert %{caller: %{}} = Caller.add(%{})
  end

  @tag :client
  test "Caller.call" do
    assert [48, %{}, "test", [], %{}] = Caller.call(%Call{procedure: "test"})
    assert [48, %{}, "test", [1], %{}] = Caller.call(%Call{procedure: "test", arg_list: [1]})

    assert [48, %{ok: 1}, "test", [1], %{test: 1}] =
             Caller.call(%Call{
               procedure: "test",
               arg_list: [1],
               arg_kw: %{test: 1},
               options: %{ok: 1}
             })
  end

  test "Peer.add" do
    assert %{} = Caller.add(%{})
  end

  test "Peer.hello" do
    assert [1, "test", %{agent: "WAMPex", roles: %{callee: %{}}}] =
             Peer.hello(%Hello{realm: "test", roles: [Callee]})
  end

  test "Peer.authenticate" do
    assert [5, "a-signa-ture", %{}] ==
             Peer.authenticate(%Authenticate{signature: "a-signa-ture", extra: %{}})
  end

  test "Peer.gooodbye" do
    assert [6, %{}, "test"] = Peer.goodbye(%Goodbye{reason: "test"})
  end

  test "Publisher.add" do
    assert %{publisher: %{}} = Publisher.add(%{})
  end

  @tag :client
  test "Publisher.publish" do
    assert [16, %{}, "test", [], %{}] = Publisher.publish(%Publish{topic: "test"})
    assert [16, %{}, "test", [1], %{}] = Publisher.publish(%Publish{topic: "test", arg_list: [1]})

    assert [16, %{test: 2}, "test", [1], %{test: 1}] =
             Publisher.publish(%Publish{
               topic: "test",
               arg_list: [1],
               arg_kw: %{test: 1},
               options: %{test: 2}
             })
  end

  test "Subscriber.add" do
    assert %{subscriber: %{}} = Subscriber.add(%{})
  end

  test "Subscriber.subscribe" do
    assert [32, %{test: 1}, "test"] =
             Subscriber.subscribe(%Subscribe{topic: "test", options: %{test: 1}})
  end

  test "Subscriber.unsubscribe" do
    assert [34, 1234] = Subscriber.unsubscribe(%Unsubscribe{subscription_id: 1234})
  end

  test "Dealer.add" do
    assert %{dealer: %{}} = Dealer.add(%{})
  end

  test "Dealer.registered" do
    assert [65, 123_456, 765_432] =
             Dealer.registered(%Registered{request_id: 123_456, registration_id: 765_432})
  end

  test "Dealer.unregistered" do
    assert [67, 123_456] = Dealer.unregistered(%Unregistered{request_id: 123_456})
  end

  test "Dealer.call_error" do
    assert [8, 48, 123_456, %{}, "error", [], %{}] =
             Dealer.call_error(%Error{request_id: 123_456, details: %{}, error: "error"})
  end

  test "Dealer.register_error" do
    assert [8, 64, 123_456, %{}, "error"] =
             Dealer.register_error(%Error{request_id: 123_456, details: %{}, error: "error"})
  end

  test "Dealer.result" do
    assert [50, 1234, %{}, [], %{}] = Dealer.result(%Result{request_id: 1234})
    assert [50, 1234, %{}, ["1"], %{}] = Dealer.result(%Result{request_id: 1234, arg_list: ["1"]})

    assert [50, 1234, %{test: 1}, [2], %{}] =
             Dealer.result(%Result{
               request_id: 1234,
               arg_list: [2],
               arg_kw: %{},
               details: %{test: 1}
             })
  end

  test "Dealer.invocation" do
    assert [68, 1234, 1234, %{}, [], %{}] =
             Dealer.invocation(%Invocation{request_id: 1234, registration_id: 1234})

    assert [68, 1234, 1234, %{}, ["1"], %{}] =
             Dealer.invocation(%Invocation{
               request_id: 1234,
               registration_id: 1234,
               arg_list: ["1"]
             })

    assert [68, 1234, 1234, %{test: 1}, [2], %{test: 2}] =
             Dealer.invocation(%Invocation{
               request_id: 1234,
               registration_id: 1234,
               arg_list: [2],
               arg_kw: %{test: 2},
               details: %{test: 1}
             })
  end

  test "Broker.add" do
    assert %{broker: %{}} = Broker.add(%{})
  end

  test "Broker.event" do
    assert [36, 123_456, 765_432, %{}, [], %{}] =
             Broker.event(%Event{subscription_id: 123_456, publication_id: 765_432})

    assert [36, 123_456, 765_432, %{}, [2], %{}] =
             Broker.event(%Event{subscription_id: 123_456, publication_id: 765_432, arg_list: [2]})

    assert [36, 123_456, 765_432, %{}, [2], %{test: 1}] =
             Broker.event(%Event{
               subscription_id: 123_456,
               publication_id: 765_432,
               arg_list: [2],
               arg_kw: %{test: 1}
             })
  end

  test "Broker.subscribed" do
    assert [33, 123_456, 123_456] =
             Broker.subscribed(%Subscribed{request_id: 123_456, subscription_id: 123_456})
  end

  test "Broker.unsubscribed" do
    assert [35, 123_456] = Broker.unsubscribed(%Unsubscribed{request_id: 123_456})
  end

  test "Broker.subscribe_error" do
    assert [8, 32, 123_456, %{}, "error"] =
             Broker.subscribe_error(%Error{request_id: 123_456, details: %{}, error: "error"})
  end

  test "Broker.published" do
    assert [17, 123_456, 654_321] =
             Broker.published(%Published{request_id: 123_456, publication_id: 654_321})
  end

  test "MessagePack Serializer" do
    assert :binary = MessagePack.data_type()

    val = [
      5,
      "this is a string/binary",
      "123456789",
      [%{"test" => "ok"}, 45, 54, 56, 77],
      %{"key" => "true", "value" => 34}
    ]

    s = MessagePack.serialize!(val)
    assert ^val = MessagePack.deserialize!(s)
  end

  test "JSON Serializer" do
    assert :text = JSON.data_type()
    assert "[5,\"123456789\",{}]" = JSON.serialize!([5, "123456789", %{}])
    assert {:ok, "[5,\"123456789\",{}]"} = JSON.serialize([5, "123456789", %{}])
    assert [5, "123456789", %{}] = JSON.deserialize!("[5,\"123456789\",{}]")
    assert {:ok, [5, "123456789", %{}]} = JSON.deserialize("[5,\"123456789\",{}]")
  end
end
