# WAMPex

You are most likely looking for the Client or Router implementation of WAMPex.

- [WAMPexClient](https://gitlab.com/entropealabs/wampex_client)
- [WAMPexRouter](https://gitlab.com/entropealabs/wampex_router)

This is a base library for [WAMPexClient](https://gitlab.com/entropealabs/wampex_client) and [WAMPexRouter](https://gitlab.com/entropealabs/wampex_router). It provides some core modules that are utilized in both libraries, such as roles, crypto and serialization.


